function showRegisterForm() {
    $('.loginBox').fadeOut('fast', function () {
        $('.registerBox').fadeIn('fast');
        $('.login-footer').fadeOut('fast', function () {
            $('.register-footer').fadeIn('fast');
        });
        $('.modal-title').html('Register with');
    });
    $('.error').removeClass('alert alert-danger').html('');
}

function showLoginForm() {
    $('#loginModal .registerBox').fadeOut('fast', function () {
        $('.loginBox').fadeIn('fast');
        $('.register-footer').fadeOut('fast', function () {
            $('.login-footer').fadeIn('fast');
        });

        $('.modal-title').html('Login with');
    });
    $('.error').removeClass('alert alert-danger').html('');
}

function openLoginModal() {
    showLoginForm();
    setTimeout(function () {
        $('#loginModal').modal('show');
    }, 230);

}

function closeModal(str)
{
    $('#'+str).modal('hide');
}

function openRegisterModal() {
    showRegisterForm();
    setTimeout(function () {
        $('#loginModal').modal('show');
    }, 230);
}

function openFileRequestModal(st) {
        $('#Requestuoloadfile').modal('show');
        var logarr={'st':st};
        $.post("./ajax/php/request_a_file.php",logarr,function (data) {
            if (data) {
                var datacl=jQuery.parseJSON(data);
                $('#Requestuoloadfile_header').html(datacl.blg_head);
                $('#Requestuoloadfile_body').html(datacl.blg_body);
            } else {
                shakeModal('Requestuoloadfile');
            }
        });
}

function openupfileexpo(str)
{
    $('input[name='+str+']').click();
}

function getFileName(str)
{
    var filename=$('input[name='+str+']').val();
    if(filename!='')
    {
        $("#place_"+str).html(filename);
    }
}

function loginAjax() {
    //   Remove this comments when moving to server
    var email = $("input[name=uemail]").val();
    var password = $("input[name=upassword]").val();
    if (email != '' && password != '')
    {
        var logarr={'st':1,'email':email,'password':password};
        $.post("./ajax/php/login.php",logarr,function (data) {
            if (data == 1) {
                window.location.replace("./homepage.php");
            } else {
                shakeModal('loginModal');
            }
        });
    }
    else
    {
        shakeModal('loginModal');
    }
}

function shakeModal(str) {
    if(str=='')
    {
       var shk='loginModal';
    }
    else
    {
        var shk=str;
    }
    $('#'+shk+' .modal-dialog').addClass('shake');
    if(str=='')
    {
        $('.error').addClass('alert alert-danger').html("Invalid email/password combination");
        $('input[type="password"]').val('');
    }
    setTimeout(function () {
        $('#'+shk+' .modal-dialog').removeClass('shake');
    }, 1000);
}

   