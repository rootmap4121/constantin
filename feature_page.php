<?php
include './class/auth.php';
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title><?php echo $fullname; ?> | Feature Page</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <link href="assets/css/landing-page.css" rel="stylesheet"/>
        <link href="assets/css/login-register.css" rel="stylesheet"/>
        <link href="assets/css/ct-navbar.css" rel="stylesheet" />
        <!-- Custom buttons and materials css starts here -->
        <link href="assets/css/custom_buttons.css" rel="stylesheet">
        <!--     Fonts and icons     -->
        <link href="assets/fonts/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    </head>
    <body class="landing-page landing-page2">
        <?php include './include/nav.php'; ?>
        <div class="wrapper">
            <div class="section section-features">
                <div class="container">
                    <div class="row">
                        <div id="f_doc" class="panel panel-info">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h1 class="panel-title"><i class="fa fa-file margin-right10"></i>Featured Documents</h1>
                                    </div>
                                    <div id="f_doc_input_group" class="col-md-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Search for...">
                                            <span id="f_doc_addon" class="input-group-btn">
                                                <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div><!-- /input-group -->
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">

                                <?php
                                $catarr=$obj->FlyQuery("SELECT * FROM feature_document_category");
                                if (!empty($catarr)) {
                                    $countdata=count($catarr);
                                    $c=1;
                                    $cc=1;
                                    foreach ($catarr as $arr):
                                        $filedataarr=$obj->FlyQuery("SELECT b.*,concat(b.first_name,' ',b.last_name) as fullname FROM upload_document_info_view as b WHERE b.file_category='" . $arr->id . "'");
                                        if ($c == 1) {
                                            ?>
                                            <div class="row"><!--here starts row-1-->
                                                <?php
                                            }
                                            ?>

                                            <div class="col-md-6">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading alpha-panel-bg3">
                                                        <div class="row">
                                                            <div class="col-md-6 col-xs3 text-left">
                                                                <h1 class="panel-title"><?php echo $arr->name; ?></h1>
                                                            </div>
                                                            <?php
                                                            if (!empty($filedataarr)) {
                                                                ?>
                                                                <div class="col-md-6 col-xs3 text-right">
                                                                    <button data-bind="<?php echo $arr->id; ?>" class="btn btn-echo btn-sm" type="button"><i class="glyphicon glyphicon-transfer margin-right10"></i> Browse More</button>
                                                                </div>
                                                                <?php
                                                            }else {
                                                                ?>
                                                                <div class="col-md-6 col-xs3 text-right">
                                                                    <button  class="btn btn-foxtrot btn-sm" type="button"><i class="glyphicon glyphicon-envelope"></i> Coming Soon, Keep in touch.</button>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="panel-body">
                                                        <?php
                                                        if (!empty($filedataarr)) {
                                                            $r=1;
                                                            $countrr=count($filedataarr);
                                                            foreach ($filedataarr as $arr2):
                                                                if ($r == $countrr) {
                                                                    ?>
                                                                    <div class="media b_btm_none">
                                                                        <?php
                                                                    }else {
                                                                        ?>
                                                                        <div class="media">
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        <div class="media-left">
                                                                            <a href="feature_page_inner.php?id=<?php echo $arr2->id; ?>" class="thumbnail">
                                                                                <img class="media-object feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                                                            </a>
                                                                        </div>
                                                                        <div class="media-body">
                                                                            <a href="feature_page_inner.php?id=<?php echo $arr2->id; ?>">
                                                                                <h5 class="media-heading pull-left margin-right10"><?php echo $arr2->document_title; ?>
                                                                                    <br/>
                                                                                    <small class="margin-right10">by <b><?php echo $arr2->fullname; ?></b></small>
                                                                                </h5>
                                                                            </a>
                                                                            <h5 class="media-heading pull-right">
                                                                                <br/>
                                                                                <small class="margin-right10"><a href=""><i class="fa fa-eye margin-right10"></i>5&nbsp;Views</a></small>
                                                                                <small><a href=""><i class="fa fa-comments margin-right10"></i>9&nbsp;Comments</a></small>
                                                                            </h5>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                    $r++;
                                                                endforeach;
                                                            }else {
                                                                ?>
                                                                <h6>No Document Available Now,<br> Although You Can Send Request For <a class="text-info" href="#" onclick="openFileRequestModal(1)">New Document</a></h6>
                                                                <?php
                                                            }
                                                            ?>



                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                if ($cc == $countdata) {
                                                    ?>
                                                </div><!--here ends row-1-->
                                                <?php
                                                $c=0;
                                            }elseif ($c == 2) {
                                                ?>
                                            </div><!--here ends row-1-->
                                            <?php
                                            $c=0;
                                        }
                                        $cc++;
                                        $c++;
                                    endforeach;
                                }
                                ?>
                                <!--BOX 1 ENDS-->
                                <!--BOX 2 ENDS-->

                                <div class="clearfix"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php
            include('./include/fotter.php');
            ?>
        </div>
    </body>
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.js" type="text/javascript"></script>
    <script src="assets/js/awesome-landing-page.js" type="text/javascript"></script>
    <script src="assets/js/login-register.js" type="text/javascript"></script>
    <script src="assets/js/ct-navbar.js"></script>
    <!--custom material js starts here-->
    <script src="assets/material/js/ripples.min.js"></script>
    <script src="assets/material/js/material.min.js"></script>
    <script>
                                                                    $(document).ready(function () {
                                                                        // This command is used to initialize some elements and make them work properly
                                                                        $.material.init();
                                                                    });
    </script>
</html>