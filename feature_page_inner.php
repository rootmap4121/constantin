<?php
include './class/auth.php';
$documentinfo=$obj->FlyQuery("SELECT b.*,concat(b.first_name,' ',b.last_name) as fullname FROM upload_document_info_view as b WHERE b.status='2' AND id='" . $_GET['id'] . "'");
if (empty($documentinfo)) {
    $obj->Error("Invalid Request, Please Try Another Docuemnt", "feature_page.php");
}else {
    ?>
    <!doctype html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <link rel="icon" type="image/png" href="assets/img/favicon.ico">
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
            <title><?php echo $fullname; ?> | Feature Document Detail | <?php echo $documentinfo[0]->document_title; ?></title>
            <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
            <meta name="viewport" content="width=device-width" />

            <link href="assets/css/bootstrap.css" rel="stylesheet" />
            <link href="assets/css/landing-page.css" rel="stylesheet"/>
            <link href="assets/css/login-register.css" rel="stylesheet"/>
            <link href="assets/css/ct-navbar.css" rel="stylesheet" />
            <!-- Custom buttons and materials css starts here -->
            <link href="assets/css/custom_buttons.css" rel="stylesheet">
            <!--     Fonts and icons     -->
            <link href="assets/fonts/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet">
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
            <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
        </head>
        <body class="landing-page landing-page2">
            <?php
            include './include/nav.php';
            ?>
            <div class="wrapper">
                <div class="section section-features">
                    <div class="container">
                        <div class="row margin-top50">
                            <div class="col-md-12"><article class="h4"><?php echo $documentinfo[0]->document_title; ?></article></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-8"><!--left panel starts here-->
                                <div class="panel panel-default">
                                    <div class="panel-heading alpha-panel-bg3">
                                        <article>
                                            <?php echo $documentinfo[0]->detail_info; ?>
                                        </article>
                                    </div>
                                    <div class="panel-heading alpha-panel-bg3">
                                        <button class="btn btn-indigo btn-sm"><i class="fa fa-pencil-square margin-right10"></i>Customize and Send for Signing</button>
                                        <div class="btn-group">

                                            <a href="./download.php?file=<?php echo $documentinfo[0]->file_name; ?>" class="btn btn-golf btn-sm dropdown-toggle" >
                                                <i class="fa fa-arrow-circle-down margin-right10"></i>Download <!--<span class="caret"></span>-->
                                            </a>
                                        </div>
                                    </div>
                                    <style>
                                        table.excel {
                                            border-style:ridge;
                                            border-width:1;
                                            border-collapse:collapse;
                                            font-family:sans-serif;
                                            font-size:12px;
                                        }
                                        table.excel thead th, table.excel tbody th {
                                            background:#CCCCCC;
                                            border-style:ridge;
                                            border-width:1;
                                            text-align: center;
                                            vertical-align:bottom;
                                        }
                                        table.excel tbody th {
                                            text-align:center;
                                            width:20px;
                                        }
                                        table.excel tbody td {
                                            vertical-align:bottom;
                                        }
                                        table.excel tbody td {
                                            padding: 0 3px;
                                            border: 1px solid #EEEEEE;
                                        }
                                    </style>
                                    <div class="panel-body">
                                        <article style="overflow-x: scroll">

                                            <?php
                                            $filetype=explode(".", $documentinfo[0]->file_name)[1];
                                            $msd=array("doc", "docx");
                                            $sxs=array("xls", "xlsx");
                                            if (in_array($filetype, $msd)) {
                                                include './excel_reader/docreader.php';
                                                $docObj=new DocxConversion("./upload/" . $documentinfo[0]->file_name);

                                                $docText=$docObj->convertToText();
                                                echo html_entity_decode($docText);
                                            }elseif (in_array($filetype, $sxs)) {
                                                error_reporting(E_ALL ^ E_NOTICE);
                                                require_once './excel_reader/dd/excel_reader2.php';
                                                $data=new Spreadsheet_Excel_Reader("./upload/" . $documentinfo[0]->file_name);

                                                echo $data->dump(true, true);
                                            }
                                            //$docObj = new DocxConversion("test.docx");
                                            //$docObj=new DocxConversion("test.xlsx");
                                            //$docObj = new DocxConversion("test.pptx");
                                            //echo html_entity_decode(file_get_contents("./upload/" . $documentinfo[0]->file_name));
                                            ?>
                                        </article>
                                    </div>
                                    <div class="panel-footer text-center">
                                        <button id="expand_fold" type="button" class="btn btn-link btn-sm">Expand Document<i class="fa fa-arrow-down margin-left10"></i></button>
                                    </div>
                                </div>
                                <div id="f_doc2" class="row margin-top20">
                                    <div class="col-md-12 bd-btm-1g"><article class="h4">Document Discussion</article></div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 margin-top15">
                                        <div class="media">
                                            <a class="pull-left" href="#">
                                                <div class="avatar">
                                                    <img class="media-object img-thumbnail img-circle com-img" src="assets/img/faces/face-1.jpg" alt="..."/>
                                                </div>
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading">Creative Tim</h4>
                                                <h6 class="pull-right text-muted">Sep 11, 11:53 AM</h6>

                                                <p>Hello guys, nice to have you on the platform! There will be a lot of great stuff coming soon. We will keep you posted for the latest news.</p>
                                                <p> Don't forget, You're Awesome!</p>

                                                <div class="media-footer">
                                                    <a href="#" class="btn btn-info btn-simple "> <i class="fa fa-reply"></i> Reply</a>
                                                    <a href="#" class="btn text-danger btn-simple pull-right">
                                                        <i class="fa fa-heart"></i> 243
                                                    </a>
                                                </div>

                                                <div class="media media-post bd-btm-1g">
                                                    <a class="pull-left author" href="#">
                                                        <div class="avatar">
                                                            <img class="media-object img-thumbnail img-circle com-img" alt="64x64" src="assets/img/faces/face-2.jpg">
                                                        </div>
                                                    </a>
                                                    <div class="media-body">
                                                        <textarea class="form-control" placeholder="Write a nice reply or go home..." rows="3" cols="96"></textarea>
                                                        <div class="media-footer">
                                                            <a href="#" class="btn btn-info btn-fill pull-right">Reply</a>
                                                        </div>
                                                    </div>
                                                </div> <!-- end media-post -->
                                            </div>
                                        </div> <!-- end media -->
                                        <div class="media">
                                            <a class="pull-left" href="#">
                                                <div class="avatar">
                                                    <img class="media-object img-thumbnail img-circle com-img" alt="Tim Picture" src="assets/img/faces/face-3.jpg">
                                                </div>
                                            </a>
                                            <div class="media-body">

                                                <h4 class="media-heading">Drake</h4>
                                                <h6 class="pull-right text-muted">Sep 11, 11:54 AM</h6>

                                                <p>Hello guys, nice to have you on the platform! There will be a lot of great stuff coming soon. We will keep you posted for the latest news.</p>
                                                <p> Don't forget, You're Awesome!</p>

                                                <div class="media-footer">
                                                    <a href="#" class="btn btn-info btn-simple "> <i class="fa fa-reply"></i> Reply</a>
                                                    <a href="#" class="btn btn-simple pull-right">
                                                        <i class="fa fa-heart-o"></i> 23
                                                    </a>
                                                </div>
                                                <div class="media">
                                                    <a class="pull-left" href="#">
                                                        <div class="avatar">
                                                            <img class="media-object img-thumbnail img-circle com-img" alt="64x64" src="assets/img/faces/face-4.jpg">
                                                        </div>
                                                    </a>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">Rick Ross</h4>
                                                        <h6 class="pull-right text-muted">Sep 11, 11:56 AM</h6>

                                                        <p>Hello guys, nice to have you on the platform! There will be a lot of great stuff coming soon. We will keep you posted for the latest news.</p>
                                                        <p> Don't forget, You're Awesome!</p>

                                                        <div class="media-footer">
                                                            <a href="#" class="btn btn-info btn-simple "> <i class="fa fa-reply"></i> Reply</a>
                                                            <a href="#" class="btn text-danger btn-simple pull-right">
                                                                <i class="fa fa-heart"></i> 43
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div> <!-- end media -->
                                            </div>
                                        </div> <!-- end media -->
                                        <div class="media">
                                            <a class="pull-left" href="#">
                                                <div class="avatar">
                                                    <img class="media-object img-thumbnail img-circle com-img" alt="64x64" src="assets/img/faces/face-4.jpg">
                                                </div>
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading">Jay Z</h4>
                                                <h6 class="pull-right text-muted">Sep 11, 11:57 AM</h6>

                                                <p>Hello guys, nice to have you on the platform! There will be a lot of great stuff coming soon. We will keep you posted for the latest news.</p>
                                                <p> Don't forget, You're Awesome!</p>

                                                <div class="media-footer">
                                                    <a href="#" class="btn btn-info btn-simple "> <i class="fa fa-reply"></i> Reply</a>
                                                    <a href="#" class="btn btn-simple pull-right">
                                                        <i class="fa fa-heart-o"></i> 24
                                                    </a>
                                                </div>
                                            </div>
                                        </div> <!-- end media -->
                                        <div class="pagination-area bd-top-1g">
                                            <ul class="pagination pagination-no-border">
                                                <!--   color-classes: "pagination-blue", "pagination-azure", "pagination-orange", "pagination-red", "pagination-green", special-classes: "pagination-no-border"  -->
                                                <li><a href="#">&laquo;</a></li>
                                                <li class="active"><a href="#">1</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">4</a></li>
                                                <li><a href="#">5</a></li>
                                                <li><a href="#">&raquo;</a></li>
                                            </ul>
                                        </div>
                                    </div><!-- /.col-lg-6 -->
                                </div>
                            </div><!--left panel ends here-->
                            <div class="col-md-4"><!--right panel starts here-->
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="fa fa-unlock margin-right10"></i>PUBLIC DOCUMENT</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="media b_btm_none pad-none">
                                            <div class="media-left">
                                                <a href="#" class="thumbnail">
                                                    <img class="media-object feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <a href="#">
                                                    <h5 class="media-heading pull-left margin-top15">Foundry Group</h5>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body b_top_info1 text-center">
                                        <button type="button" class="btn btn-alpha btn-sm" data-toggle="tooltip" data-placement="top" title="Number of times Signed"><i class="fa fa-pencil margin-right5"></i>11</button>
                                        <button type="button" class="btn btn-mike btn-sm" data-toggle="tooltip" data-placement="top" title="Number of Saves"><i class="fa fa-star margin-right5"></i>50</button>
                                        <button type="button" class="btn btn-golf btn-sm" data-toggle="tooltip" data-placement="top" title="Number of Downloads"><i class="fa fa-download margin-right5"></i>1.1k</button>
                                        <button type="button" class="btn btn-indigo btn-sm" data-toggle="tooltip" data-placement="top" title="Number of Views"><i class="fa fa-eye margin-right5"></i>24.2k</button>
                                    </div>
                                    <div class="panel-body b_top_info1 text-center">
                                        <span class="label label-danger">term-sheet-bundle</span>
                                        <span class="label label-danger">founder-bundle</span>
                                        <span class="label label-danger">startup</span>
                                        <span class="label label-danger">funding</span>
                                        <span class="label label-danger">termsheet</span>
                                        <span class="label label-danger">financing</span>
                                        <span class="label label-danger">venture capital</span>
                                    </div>
                                    <div class="panel-body b_top_info1 text-left">
                                        <article>This is <a href="">version 5</a>, from 3 years ago.</article>
                                    </div>
                                    <div class="panel-body b_top_info1 text-left">
                                        <article>Suggest changes by making a copy of this document. <a href="">Learn more</a>.</article>
                                        <button type="button" class="btn btn-orange btn-sm margin-top5"><i class="fa fa-plus-square margin-right5"></i> Create Branch</button>
                                    </div>
                                    <div class="panel-body b_top_info1 text-left">
                                        <button type="button" class="btn btn-orange btn-sm margin-top5"><i class="fa fa-heart margin-right5"></i> Love this document</button>
                                        <article class="margin-top5">They Love This Document:</article>
                                        <div class="clearfix margin-top5"></div>
                                        <a href="#" class="thumbnail love">
                                            <img class="feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                        </a>
                                        <a href="#" class="thumbnail love">
                                            <img class="feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                        </a>
                                        <a href="#" class="thumbnail love">
                                            <img class="feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                        </a>
                                        <a href="#" class="thumbnail love">
                                            <img class="feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                        </a>
                                        <a href="#" class="thumbnail love">
                                            <img class="feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                        </a>
                                        <a href="#" class="thumbnail love">
                                            <img class="feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                        </a>
                                        <a href="#" class="thumbnail love">
                                            <img class="feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                        </a>
                                        <a href="#" class="thumbnail love">
                                            <img class="feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                        </a>
                                        <a href="#" class="thumbnail love">
                                            <img class="feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                        </a>
                                        <a href="#" class="thumbnail love">
                                            <img class="feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                        </a>
                                        <a href="#" class="thumbnail love">
                                            <img class="feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                        </a>
                                        <a href="#" class="thumbnail love">
                                            <img class="feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                        </a>

                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h1 class="panel-title">Related Documents</h1>
                                    </div>
                                    <div class="panel-body">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#" class="thumbnail">
                                                    <img class="media-object feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <a href="#">
                                                    <h5 class="media-heading pull-left margin-right10">Foundry Group Standard Termsheet
                                                        <br/>
                                                        <small class="margin-right10">by <b>Foundry Group</b></small>
                                                    </h5>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#" class="thumbnail">
                                                    <img class="media-object feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <a href="#">
                                                    <h5 class="media-heading pull-left margin-right10">Foundry Group Standard Termsheet
                                                        <br/>
                                                        <small class="margin-right10">by <b>Foundry Group</b></small>
                                                    </h5>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="media b_btm_none">
                                            <div class="media-left">
                                                <a href="#" class="thumbnail">
                                                    <img class="media-object feat_img" src="assets/img/smalls/aiga.jpg" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <a href="#">
                                                    <h5 class="media-heading pull-left margin-right10">Foundry Group Standard Termsheet
                                                        <br/>
                                                        <small class="margin-right10">by <b>Foundry Group</b></small>
                                                    </h5>
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div><!--right panel ends here-->
                        </div>
                    </div><!--section features container ends here-->
                </div><!--section features ends here-->
                <div class="clearfix"></div>
                <?php include './include/fotter.php'; ?>
            </div>
            <script>
                $(document).ready(function () {
                    $("#folded-txt").hide();
                    $("#expand_fold").click(function () {
                        $("#folded-txt").slideToggle();
                    });
                });

                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                })
            </script>
        </body>
        <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
        <script src="assets/js/bootstrap.js" type="text/javascript"></script>
        <script src="assets/js/awesome-landing-page.js" type="text/javascript"></script>
        <script src="assets/js/login-register.js" type="text/javascript"></script>
        <script src="assets/js/ct-navbar.js"></script>
        <!--custom material js starts here-->
        <script src="assets/material/js/ripples.min.js"></script>
        <script src="assets/material/js/material.min.js"></script>
        <script>
                $(document).ready(function () {
                    // This command is used to initialize some elements and make them work properly
                    $.material.init();
                });
        </script>
    </html>
<?php } ?>