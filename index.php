<?php
include './class/config.php';
$ft=$obj->FlyQuery("SELECT title,content_short FROM home_focus_content_upper");
$fc=$obj->FlyQuery("SELECT name,profession,photo,comment FROM what_people_think");
$team=$obj->FlyQuery("SELECT name,designation,address,company_name,email_address,experience,area_of_expertise,comment,photo,facebook_social_link,google_plus_social_link,twitter_social_link FROM team_info");
$client_info=$obj->FlyQuery("SELECT name,logo FROM client_info");
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Constantin Vermoere</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <link href="assets/css/landing-page.css" rel="stylesheet"/>
        <link href="assets/css/login-register.css" rel="stylesheet"/>
        <link href="assets/css/rotating-card.css" rel="stylesheet" />
        <link href="assets/css/custom_buttons.css" rel="stylesheet">
        <!--     Fonts and icons     -->
        <link href="assets/fonts/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
        <script src="ajax/json/script.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    </head>
    <body class="landing-page landing-page2">
        <span style="position: absolute; z-index: 99999; top: 5px; right:5px;" id="modal_message"></span>
        <nav class="navbar navbar-transparent navbar-top" role="navigation" id="main-nav">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button id="menu-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a href="#">
                        <div class="logo-container">
                            <div class="logo">
                                <img src="assets/img/Costa01.png" alt="Creative Tim Logo">
                            </div>
                            <div class="brand">
                                Constantin Vermoere
                            </div>
                        </div>
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="example" >
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#">
                                <i class="fa fa-facebook-square"></i>
                                Like
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                                Tweet
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-pinterest"></i>
                                Pin
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
        </nav>
        <div class="wrapper">
            <div class="parallax filter-gradient blue" data-color="blue">
                <div class= "container">
                    <div class="row">
                        <div class="col-md-7  hidden-xs">
                            <div class="parallax-image">
                                <img src="assets/img/showcases/showcase-2/mac1.png"/>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="description text-center">
                                <h2><?php echo $ft[0]->title; ?></h2>
                                <br>
                                <h5><?php echo $ft[0]->content_short; ?></h5>
                                <div class="buttons">
                                    <button class="btn btn-fill btn-neutral" data-toggle="modal" href="javascript:void(0)" onclick="openRegisterModal();">
                                        <i class="fa fa-user-plus"></i>&nbsp;&nbsp; Sign Up
                                    </button>
                                    <button class="btn btn-fill btn-neutral" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">
                                        Log In &nbsp;&nbsp;<i class="fa fa-lock"></i>
                                    </button>
                                    <!--Here starts modal form-->
                                    <div class="modal fade login" id="loginModal">
                                        <div class="modal-dialog login animated">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button  type="button" class="close" data-dismiss="modal"  aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Login with</h4>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="box">
                                                        <div class="content">
                                                            <div class="social">
                                                                <a id="google_login" class="circle google" href="#">
                                                                    <i class="fa fa-google-plus fa-fw"></i>
                                                                </a>
                                                                <a id="linkedin_login" class="circle linkedin" href="#">
                                                                    <i class="fa fa-linkedin fa-fw"></i>
                                                                </a>
                                                                <a id="facebook_login" class="circle facebook" href="#">
                                                                    <i class="fa fa-facebook fa-fw"></i>
                                                                </a>
                                                            </div>
                                                            <div class="division">
                                                                <div class="line l"></div>
                                                                <span>or</span>
                                                                <div class="line r"></div>
                                                            </div>
                                                            <div class="error"></div>
                                                            <div class="form loginBox">
                                                                <form method="post" action="/login" accept-charset="UTF-8">
                                                                    <input id="email" class="form-control" type="text" placeholder="Email" name="uemail">
                                                                    <input id="password" class="form-control" type="password" placeholder="Password" name="upassword">
                                                                    <input class="btn btn-default btn-login" type="button" value="Login" onclick="loginAjax()">
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box">
                                                        <div class="content registerBox" style="display:none;">
                                                            <div class="form">
                                                                <form method="post" html="{:multipart=>true}" data-remote="true" action="/register" accept-charset="UTF-8">
                                                                    <input id="full_name" class="form-control" type="text" placeholder="Full Name" name="full_name">
                                                                    <input id="email" class="form-control" type="text" placeholder="Email" name="semail">
                                                                    <input id="password" class="form-control" type="password" placeholder="Password" name="spassword">
                                                                    <input id="password_confirmation" class="form-control" type="password" placeholder="Repeat Password" name="spassword_confirmation">
                                                                    <input class="btn btn-default btn-register" type="button" value="Create account" name="commit">
                                                                </form>
                                                            </div>
                                                            <script>
                                                                $(document).ready(function () {
                                                                    $("input[value='Create account']").click(function () {
                                                                        var fullname = $("input[name=full_name]").val();
                                                                        var email = $("input[name=semail]").val();
                                                                        var password = $("input[name=spassword]").val();
                                                                        var password_confirmation = $("input[name=spassword_confirmation]").val();
                                                                        if (fullname == '' || email == '' || password == '' || password_confirmation == '')
                                                                        {
                                                                            var msg = Error("Some Field is Empty");
                                                                            $("#modal_message").html(msg);
                                                                            hideMessage("hidebox");
                                                                        }
                                                                        else
                                                                        {
                                                                            if (password == password_confirmation)
                                                                            {
                                                                                var arr = {'st': 1, 'fullname': fullname, 'email': email, 'password': password};
                                                                                $.post("./ajax/php/signup.php", arr, function (data) {
                                                                                    if (data == 1)
                                                                                    {
                                                                                        var msg = Success('You are successfully registered.');
                                                                                        $("#modal_message").html(msg);
                                                                                        $("#loginModal").modal('hide');
                                                                                        hideMessage("hidebox");
                                                                                    }
                                                                                    else if (data == 2)
                                                                                    {
                                                                                        var msg = Error('Failed to insert, Please Try Again.');
                                                                                        $("#modal_message").html(msg);
                                                                                        hideMessage("hidebox");
                                                                                    }
                                                                                    else if (data == 3)
                                                                                    {
                                                                                        var msg = Warning('Email already exists');
                                                                                        $("#modal_message").html(msg);
                                                                                        hideMessage("hidebox");
                                                                                    }
                                                                                    else if (data == 4)
                                                                                    {
                                                                                        var msg = Error('Fields is empty');
                                                                                        $("#modal_message").html(msg);
                                                                                        hideMessage("hidebox");
                                                                                    }
                                                                                });
                                                                                //var msg = Success("Validation True");
                                                                            }
                                                                            else
                                                                            {
                                                                                var msg = Warning("Password Mismatch");
                                                                                $("#modal_message").html(msg);
                                                                                hideMessage("hidebox");
                                                                            }
                                                                        }
                                                                    });
                                                                });
                                                            </script>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="forgot login-footer">
                                                        <span>Looking to
                                                            <a href="javascript: showRegisterForm();">create an account</a>
                                                            ?</span>
                                                    </div>
                                                    <div class="forgot register-footer" style="display:none">
                                                        <span>Already have an account?</span>
                                                        <a href="javascript: showLoginForm();">Login</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--here ends modal form-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-gray section-clients">
                <div class="container text-center">
                    <h4 class="header-text"><?php echo $ft[1]->title; ?></h4>
                    <p>
                        <?php echo $ft[1]->content_short; ?>
                    </p>
                    <div class="logos">
                        <ul class="list-unstyled">
                            <?php
                            if (count($client_info) > 0) {
                                foreach ($client_info as $info):
                                    ?>
                                    <li><img src="./constantin_admin/upload/<?php echo $info->logo; ?>"/></li>
                                    <?php
                                endforeach;
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="section section-presentation">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 hidden-xs">
                            <div class="description">
                                <h4 class="header-text"><?php echo $ft[2]->title; ?></h4>
                                <p><?php echo $ft[2]->content_short; ?></p>
                                <ol>

                                </ol>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <img src="assets/img/showcases/showcase-2/mac2.png" style="margin-top:-50px"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-demo">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="demo-image">
                                <img src="assets/img/showcases/showcase-2/examples/home_4.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            <h4 class="header-text"><?php echo $ft[3]->title; ?></h4>
                            <p><?php echo $ft[3]->content_short; ?></p>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-5">
                            <h4 class="header-text"><?php echo $ft[4]->title; ?></h4>
                            <p><?php echo $ft[4]->content_short; ?></p>
                        </div>
                        <div class="col-md-6 col-md-offset-1">
                            <div class="demo-image">
                                <img src="assets/img/showcases/showcase-2/examples/home_6.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-testimonial">
                <div class="container">
                    <h4 class="header-text text-center">What people think</h4>
                    <div id="carousel-example-generic" class="carousel fade" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">


                            <?php
                            if (count($fc) > 0) {
                                $i=1;
                                foreach ($fc as $cc):
                                    ?>
                                    <div class="item<?php if ($i == 1) { ?> active<?php } ?>">
                                        <div class="mask">
                                            <img src="./constantin_admin/upload/<?php echo $cc->photo; ?>">
                                        </div>
                                        <div class="carousel-testimonial-caption">
                                            <p><?php echo $cc->name; ?>, <?php echo $cc->profession; ?> </p>
                                            <h3>"<?php echo $cc->comment; ?>"</h3>
                                        </div>
                                    </div>
                                    <?php
                                    $i++;
                                endforeach;
                            }
                            ?>
                        </div>
                        <ol class="carousel-indicators carousel-indicators-blue">
                            <?php
                            if (count($fc) > 0) {
                                $i=0;
                                foreach ($fc as $cc):
                                    ?>
                                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>"<?php if ($i == 0) { ?> class="active"<?php } ?>></li>
                                    <?php
                                    $i++;
                                endforeach;
                            }
                            ?>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="section section-no-padding rotate-card-body"><!--starts section for ratating card-->
                <div class="container">
                    <div class="row">
                        <h1 class="title">
                            This is our awesome team
                            <br>
                            <small>Present your team in an interesting way</small>
                        </h1>
                        <div class="col-sm-10 col-sm-offset-1">
                            <?php
                            if (count($team) > 0) {
                                foreach ($team as $tm):
                                    ?>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="card-container">
                                            <div class="card">
                                                <div class="front">
                                                    <div class="cover">
                                                        <img src="assets/img/card/rotating_card_thumb2.png"/>
                                                    </div>
                                                    <div class="user">
                                                        <img class="img-circle" src="./constantin_admin/upload/<?php echo $tm->photo; ?>"/>
                                                    </div>
                                                    <div class="content">
                                                        <div class="main">
                                                            <h4 class="name"><?php echo $tm->name; ?></h4>
                                                            <p class="profession"><?php echo $tm->designation; ?></p>
                                                            <h6><i class="fa fa-map-marker fa-fw text-muted"></i> <?php echo $tm->address; ?></h6>
                                                            <h6><i class="fa fa-building-o fa-fw text-muted"></i> <?php echo $tm->company_name; ?> </h6>
                                                            <h6><i class="fa fa-envelope-o fa-fw text-muted"></i> <?php echo $tm->email_address; ?></h6>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> <!-- end front panel -->
                                                <div class="back">
                                                    <div class="header">
                                                        <h6 class="motto">"<?php echo $tm->comment; ?>"</h6>
                                                    </div>
                                                    <div class="content">
                                                        <div class="main">
                                                            <h5 class="text-center">Experince</h5>
                                                            <p><?php echo $tm->experience; ?></p>
                                                            <h5 class="text-center">Areas of Expertise</h5>
                                                            <p><?php echo $tm->area_of_expertise; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="social-links text-center">
                                                            <a href="<?php echo $tm->facebook_social_link; ?>" class="facebook"><i class="fa fa-facebook fa-fw"></i></a>
                                                            <a href="<?php echo $tm->google_plus_social_link; ?>" class="google"><i class="fa fa-google-plus fa-fw"></i></a>
                                                            <a href="<?php echo $tm->twitter_social_link; ?>" class="twitter"><i class="fa fa-twitter fa-fw"></i></a>
                                                        </div>
                                                    </div>
                                                </div> <!-- end back panel -->
                                            </div> <!-- end card -->
                                        </div> <!-- end card-container -->
                                    </div> <!-- end col sm 3 -->
                                    <?php
                                endforeach;
                            }
                            ?>
                            <!--         <div class="col-sm-1"></div> -->
                            <!-- end col sm 3 -->
                            <!--         <div class="col-sm-1"></div> -->

                        </div> <!-- end col-sm-10 -->
                    </div> <!-- end row -->
                    <div class="space-200"></div>
                </div>
            </div>
            <div class="section section-no-padding">
                <div class="parallax filter-gradient blue" data-color="blue">
                    <div class="parallax-background">
                        <img class ="parallax-background-image" src="assets/img/showcases/showcase-2/bg2.jpg">
                    </div>
                    <div class="info">
                        <h1>Join us now!</h1>
                        <p>.</p>
                        <button href="#" class="btn btn-neutral btn-lg btn-fill" data-toggle="modal" href="javascript:void(0)" onclick="openRegisterModal();">JOIN</button>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

            <?php include('./include/fotter.php'); ?>


        </div>
        <script src="assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
        <script src="assets/js/bootstrap.js" type="text/javascript"></script>
        <script src="assets/js/awesome-landing-page.js" type="text/javascript"></script>
        <script src="assets/js/login-register.js" type="text/javascript"></script>
        <script type="text/javascript">
                            $('document').ready(function ()
                            {
                                $('button .close').click(function () {
                                    alert('Success');
                                });

                            });

                            function rotateCard(btn) {
                                var $card = $(btn).closest('.card-container');
                                console.log($card);
                                if ($card.hasClass('hover')) {
                                    $card.removeClass('hover');
                                } else {
                                    $card.addClass('hover');
                                }
                            }
        </script>

    </body>
</html>