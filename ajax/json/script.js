/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function hideMessage(msg)
{
    setTimeout(function () {
        $("#"+msg).hide('slow');
    }, 3000);
}


function Message(MClass, Type, Message)
{
    $("#hidebox").show('slow');
    var template = "<div id='hidebox' class='alert alert-" + MClass + " alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>" + Type + " </strong> " + Message + "</div>";
    return template;
    
    
}

function Success(msg)
{
    var mass = Message("success", "<i class='fa fa-check-circle'></i>", msg);
    
    return mass;
}

function Error(msg)
{
    var mass = Message("danger", "<i class='fa fa-minus-circle'></i>", msg);
    return mass;
}

function Warning(msg)
{
    var mass = Message("warning", "<i class='fa fa-exclamation-triangle'></i>", msg);
    return mass;
}

function getupfile()
{
    var filen=$('input[name=upfile]').val();
    if(filen!='')
    {
        $("#place_upfile").html(filen);
        $("#place_upfile").addClass('text-success');
    }
}


























