<?php

include('../../class/config.php');
extract($_POST);
$filetype='';
$sqlstring="SELECT id,name FROM feature_document_category";
$querystring=$obj->FlyQuery($sqlstring);
if (count($querystring) > 0) {
    $filetype .='<option value="">Please Select</option>';
    foreach ($querystring as $tp):
        $filetype .='<option value="' . $tp->id . '">' . $tp->name . '</option>';
    endforeach;
}
if ($st == 1) {
    $header='';
    $header .='<button  type="button" class="close" data-dismiss="modal"  aria-hidden="true">&times;</button>
                            <blockquote class="pull-left text-success">
        <p>Please Fillup All Info & Submit Your Query </p>
      </blockquote>
      <div class="clearfix"></div>';
    $body='';
    $body .='<form class="form-horizontal">
<div class="form-group">
    <label for="inputEmail3" class="col-sm-4 control-label">Your Fullname</label>
    <div class="col-sm-6">
      <input type="text" style="color:#000; background:#fff; font-weight:300;" name="fullname" class="form-control" id="fullname" placeholder="Type Your Full Name">
    </div>
  </div>
<div class="form-group">
    <label for="inputEmail3" class="col-sm-4 control-label">Your Email</label>
    <div class="col-sm-6">
      <input type="email" style="color:#000; background:#fff; font-weight:300;" name="email" class="form-control" id="inputEmail3" placeholder="Email">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-4 control-label">Your File Type</label>
    <div class="col-sm-6">
      <select style="color:#000; font-weight:300; background:#fff;" name="cid" class="form-control">
        ' . $filetype . '
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-4 control-label">Detail Info</label>
    <div class="col-sm-6">
    <textarea style="color:#000; background:#fff; font-weight:300;" name="info" placeholder="Detail of your requested file?" class="form-control" rows="3"></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-offset-2 col-sm-4">
      <button type="button"  onclick="openFileRequestModal(2)" class="btn btn-warning pull-left"><i class="fa fa-search-plus"></i>
 Track A Requested File</button>
    </div>
    <div class="col-sm-4">
      <button type="button" name="save_file_request" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>
 Submit</button>
    </div>
  </div>
</form>
<script>
            $(document).ready(function () {

                var baseurl = "' . $obj->baseUrl() . '";

                $("button[name=save_file_request]").click(function () {
                    var fullname = $("input[name=fullname]").val();
                    var email = $("input[name=email]").val();
                    var cid = $("select[name=cid]").val();
                    var info = $("textarea[name=info]").val();
                    //console.log(fullname);
                    $.post(baseurl + "ajax/php/processing_file.php", {"st": 1,"fullname": fullname,"email":email,"cid":cid,"info":info}, function (data) {
                        if(data==1)
                        {
                            alert("Your Requested Information Saved Successfully, You Will Notify Shortly.");
                            $("#Requestuoloadfile").modal("hide");
                        }
                        else if(data==2)
                        {
                            alert("Failed, Please Try Again.");
                        }
                        else if(data==0)
                        {
                            alert("Empty, Please Fillup All Field.");
                        }
                    });
                });
            });
        </script>';

    $array=array('blg_head'=>$header, 'blg_body'=>$body);
    echo json_encode($array);
}elseif ($st == 2) {
    $header='';
    $header .='<button  type="button" class="close" data-dismiss="modal"  aria-hidden="true">&times;</button>
                            <blockquote class="pull-left text-success">
        <p>Please Give Your Email Address </p>
      </blockquote>
      <div class="clearfix"></div>
';
    $body='';
    $body .='<form class="form-horizontal">
<div class="form-group">
    <label for="inputEmail3" class="col-sm-4 control-label">Your Email</label>
    <div class="col-sm-6">
      <input type="email" style="color:#000; background:#fff; font-weight:300;" name="email" class="form-control" id="inputEmail3" placeholder="Email">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-4 control-label">Your File Type</label>
    <div class="col-sm-6">
      <select style="color:#000; font-weight:300; background:#fff;" name="cid" class="form-control">
        ' . $filetype . '
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-offset-2 col-sm-4">
      <button type="button"  onclick="openFileRequestModal(1)" class="btn btn-info pull-left"><i class="fa fa-search-plus"></i>
 Requested For A New File</button>
    </div>
    <div class="col-sm-4">
      <button type="button" name="check_track_status" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i>
 Track Status</button>
    </div>
  </div>
</form><script>
            $(document).ready(function () {

                var baseurl = "' . $obj->baseUrl() . '";

                $("button[name=check_track_status]").click(function () {
                    var email = $("input[name=email]").val();
                    var cid = $("select[name=cid]").val();
                    //console.log(fullname);
                    $.post(baseurl + "ajax/php/processing_file.php", {"st": 2,"email":email,"cid":cid}, function (data) {
                        if(data==1)
                        {
                            alert("Your Requested Information Uploaded, Please Browse selected category to get your expected file.");
                            $("#Requestuoloadfile").modal("hide");
                        }
                        else if(data==2)
                        {
                            alert("Your Requested Information is not Uploaded yet, Please Browse selected category to get your expected file.");
                            $("#Requestuoloadfile").modal("hide");
                        }
                        else if(data==0)
                        {
                            alert("Empty, Please Fillup Both Field.");
                        }
                    });
                });
            });
        </script>';

    $array=array('blg_head'=>$header, 'blg_body'=>$body);
    echo json_encode($array);
}elseif ($st == 3) {

    $fl="'upfile'";
    $fl_place="upfile";

    $header='';
    $header .='<button  type="button" class="close" data-dismiss="modal"  aria-hidden="true">&times;</button>
                            <blockquote class="pull-left text-success">
        <p>Please Fillup All Info & Upload Your File </p>
      </blockquote>
      <div class="clearfix"></div>';

    $body='';
    $body .='<form class="form-horizontal" enctype="multipart/form-data" name="rfn_rt" id="rfn_rt" method="post">
<div class="form-group">
    <label for="inputEmail3" class="col-sm-4 control-label">Your Fullname</label>
    <div class="col-sm-6">
      <input type="text" name="fullname" style="color:#000; background:#fff; font-weight:300;" class="form-control" id="inputEmail3" placeholder="Type Your Full Name">
</div>
</div>
<div class="form-group">
<label for="inputEmail3" class="col-sm-4 control-label">Your Email</label>
<div class="col-sm-6">
<input type="email" name="email" style="color:#000; background:#fff; font-weight:300;" class="form-control" id="inputEmail3" placeholder="Email">
</div>
</div>
<div class="form-group">
<label for="inputPassword3" class="col-sm-4 control-label">Your File Type</label>
<div class="col-sm-6">
<select style="color:#000; font-weight:300; background:#fff;" name="cid" class="form-control">
"' . $filetype . '"
</select>
</div>
</div>
<div class="form-group">
<label for="inputEmail3" class="col-sm-4 control-label">Document Title</label>
<div class="col-sm-6">
<input type="text" name="document_title" style="color:#000; background:#fff; font-weight:300;" class="form-control" id="inputEmail3" placeholder="Document Title">
</div>
</div>
<div class="form-group">
<label for="inputPassword3" class="col-sm-4 control-label">Your File Type</label>
<div class="col-sm-6">
<a class="btn btn-indigo" onclick="openupfileexpo(' . $fl . ')" id="uploadfile" type="button"><i class="glyphicon glyphicon-upload margin-right10"></i>Upload Document</a>
<span style="opacity:0;
height:0px;
width:0px;
overflow:hidden;
display:none;
">
<input type="file" onchange="getupfile()" name="upfile" id="upfile">
</span>
<span id="place_upfile"></span>
</div>
</div>
<div class="form-group">
<label for="inputEmail3" class="col-sm-4 control-label">Detail Info</label>
<div class="col-sm-6">
<textarea style="color:#000; background:#fff; font-weight:300;" name="info" placeholder="Detail of your requested file?" class="form-control" rows="3"></textarea>
</div>
</div>
<div class="form-group">

<div class="col-sm-4">
<button type="button" name="file_request_new" class="btn btn-info pull-right"><i class="fa fa-floppy-o"></i>
Submit</button>
</div>
</div>
</form><script src="http://malsup.github.com/jquery.form.js"></script>';
    $ff="<i class='fa fa-floppy-o'></i>";
    $body .='<script>
$(document).ready(function(){
    $("button[name=file_request_new]").click(function(){
    var baseurl="' . $obj->baseUrl() . '";
        var form=$("form[name=rfn_rt]");
        console.log("submitted");

            var options = {
            url: baseurl + "ajax/php/processing_file_upload.php",
            type: "post",
            dataType: "json",
            success: function (response) {
               if(response==1)
               {
                    alert("Document Saved Successfully");
                    $("#Requestuoloadfile").modal("hide");
               }
               else if(response==2)
               {
                    alert("Failed To Store Document, Please Try Again.");
               }
               else if(response==0)
               {
                    alert("Warning, Please Fillup All Field.");
               }
            }
        };
        $("#rfn_rt").ajaxSubmit(options);
});
});
</script>';

    $array=array('blg_head'=>$header, 'blg_body'=>$body);
    echo json_encode($array);
}
?>
