<?php

header('Content-disposition: inline');
header('Content-type: application/msword');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './class/auth.php';
// grab the requested file's name
$file_name=$_GET['file'];

// make sure it's a file before doing anything!
if (is_file($file_name)) {
    // not sure if this is the correct MIME type
    readfile($file_name);
    exit;
}