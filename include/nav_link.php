<a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <i class="pe-7s-user"></i>
    <p>Account</p>
</a>
<ul class="dropdown-menu">
    <li><a href="./homepage.php"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="./feature_page.php"><i class="fa fa-lightbulb-o"></i> Feature Content</a></li>
    <li><a href="./logout.php"><i class="fa fa-unlock-alt"></i> Logout</a></li>
</ul>