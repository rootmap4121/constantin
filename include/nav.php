<span style="position: absolute; z-index: 99999; top: 5px; right:5px;" id="modal_message"></span>
<?php
$exstyle=array("feature_page.php", "feature_page_inner.php");
if (in_array($obj->filename(), $exstyle)) {
    ?>
    <nav class="navbar navbar-ct-blue navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#">
                    <div class="logo-container">
                        <div class="logo">
                            <img src="assets/img/Costa01.png" alt="Creative Tim Logo">
                        </div>
                        <div class="brand">
                            <?php echo $fullname; ?>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#">
                            <i class="pe-7s-global"></i>
                            <p>Explore</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="pe-7s-mail">
                                <span class="label">23</span>
                            </i>
                            <p>Messages</p>
                        </a>
                    </li>
                    <li class="dropdown">
                        <?php include 'include/nav_link.php'; ?>
                    </li>
                </ul>
                <form class="navbar-form navbar-right navbar-search-form" role="search">
                    <div class="form-group">
                        <input type="text" value="" class="form-control" placeholder="Search...">
                    </div>
                </form>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <?php
}else {
    ?>
    <nav class="navbar navbar-ct-blue navbar-fixed-top navbar-transparent btn-info" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#">
                    <div class="logo-container">
                        <div class="logo">
                            <img src="assets/img/Costa01.png" alt="Creative Tim Logo">
                        </div>
                        <div class="brand">
                            <?php echo $fullname; ?>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#">
                            <i class="pe-7s-global"></i>
                            <p>Explore</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="pe-7s-mail">
                                <span class="label">23</span>
                            </i>
                            <p>Messages</p>
                        </a>
                    </li>
                    <li class="dropdown">
                        <?php include 'include/nav_link.php'; ?>
                    </li>
                </ul>
                <form class="navbar-form navbar-right navbar-search-form" role="search">
                    <div class="form-group">
                        <input type="text" value="" class="form-control" placeholder="Search...">
                    </div>
                </form>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
<?php } ?>