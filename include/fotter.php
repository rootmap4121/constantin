<!--Here starts modal form-->
<div class="modal fade" id="Requestuoloadfile">
    <div class="modal-dialog login animated">
        <div class="modal-content">
            <div class="modal-header"  id="Requestuoloadfile_header"></div>
            <div class="modal-body"  id="Requestuoloadfile_body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<!--here ends modal form-->
<section class="footer-top btn-oscar">
    <div class="container">
        <div class="row margin-top50">
            <div class="col-md-9 footer-grid">
                <div class="col-md-3">
                    <div class="text-left bd-dsh-btm-1g">
                        <label class="h5 pad-5"><i class="glyphicon glyphicon-user margin-right10"></i><strong>Personal</strong></label>
                    </div>
                    <div class="clearfix margin-top15"></div>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Real Estate</a></label><br>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Will</a></label><br>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Power of Attorney</a></label><br>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Personal Property</a></label><br>
                </div>
                <div class="col-md-3">
                    <div class="text-left bd-dsh-btm-1g">
                        <label class="h5 pad-5"><i class="glyphicon glyphicon-briefcase margin-right10"></i><strong>Business</strong></label>
                    </div>
                    <div class="clearfix margin-top15"></div>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Non-Disclosure</a></label><br>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Employment</a></label><br>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Sale and Purchase</a></label><br>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Consulting</a></label><br>
                </div>
                <div class="col-md-3">
                    <div class="text-left bd-dsh-btm-1g">
                        <label class="h5  pad-5"><i class="glyphicon glyphicon-gift margin-right10"></i><strong>More Bundles</strong></label>
                    </div>
                    <div class="clearfix margin-top15"></div>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Freelancers</a></label><br>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Developers</a></label><br>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Hackathon</a></label><br>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Personal</a></label><br>
                </div>
                <div class="col-md-3">
                    <div class="text-left bd-dsh-btm-1g">
                        <label class="h5  pad-5"><i class="glyphicon glyphicon-folder-open margin-right10"></i><strong>All Documents</strong></label>
                    </div>
                    <div class="clearfix margin-top15"></div>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Most Signatures</a></label><br>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Most Views</a></label><br>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Most Recent</a></label><br>
                    <label><a href="#" class="footer-link"><i class="glyphicon glyphicon-link margin-right10"></i>Show All Tags</a></label><br>
                </div>
            </div><!--col-md-9 ends here-->
            <div class="col-md-3 text-center pad-btm-20 box-shadow-inset" style="background-color: #414436; margin-top: 50px;">
                <br>
                <a onclick="openFileRequestModal(3)" class="btn btn-indigo" type="button"><i class="glyphicon glyphicon-upload margin-right10"></i>Upload a Document</a>
                <br>
                <label>or</label>
                <br>
                <a  onclick="openFileRequestModal(1)" class="btn btn-golf" type="button"><i class="glyphicon glyphicon-question-sign margin-right10"></i>Request a Document</a>
                <br>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<footer class="footer">
    <div class="container">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="#">
                        Home
                    </a>
                </li>
                <li>
                    <a href="#">
                        Company
                    </a>
                </li>
                <li>
                    <a href="#">
                        Portfolio
                    </a>
                </li>
                <li>
                    <a href="#">
                        Blog
                    </a>
                </li>
            </ul>
        </nav>
        <div class="social-area pull-right">
            <a class="btn btn-social btn-facebook btn-simple">
                <i class="fa fa-facebook-square"></i>
            </a>
            <a class="btn btn-social btn-twitter btn-simple">
                <i class="fa fa-twitter"></i>
            </a>
            <a class="btn btn-social btn-pinterest btn-simple">
                <i class="fa fa-pinterest"></i>
            </a>
        </div>
        <div class="copyright">
            &copy; 2015 <a class="footer-link" href="#">Constantin Vermoere</a>, made with love
        </div>
    </div>
</footer>