<?php
include './class/auth.php';
$ft=$obj->FlyQuery("SELECT title,content_short FROM home_focus_content_upper");
$fc=$obj->FlyQuery("SELECT name,icon_class,short_content FROM feature_content");
$client_info=$obj->FlyQuery("SELECT name,logo FROM client_info");
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title><?php echo $fullname; ?> - Home</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <link href="assets/css/landing-page.css" rel="stylesheet"/>
        <link href="assets/css/login-register.css" rel="stylesheet"/>
        <link href="assets/css/ct-navbar.css" rel="stylesheet" />
        <link href="assets/css/custom_buttons.css" rel="stylesheet">
        <!--     Fonts and icons     -->
        <link href="assets/fonts/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="ajax/json/script.js"></script>
    </head>
    <body class="landing-page landing-page2">
        <?php
        include('./include/nav.php');
        ?>
        <div class="wrapper">
            <div class="parallax filter-gradient blue" data-color="blue">
                <div class= "container">
                    <div class="row">
                        <div class="col-md-7  hidden-xs">
                            <div class="parallax-image">
                                <img src="assets/img/showcases/showcase-2/mac1.png"/>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="description text-center">
                                <h2><?php echo $ft[0]->title; ?></h2>
                                <br>
                                <h5><?php echo $ft[0]->content_short; ?></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-gray section-clients">
                <div class="container text-center">
                    <h4 class="header-text"><?php echo $ft[1]->title; ?></h4>
                    <p>
                        <?php echo $ft[1]->content_short; ?>
                    </p>
                    <div class="logos">
                        <ul class="list-unstyled">
                            <?php
                            if (count($client_info) > 0) {
                                foreach ($client_info as $info):
                                    ?>
                                    <li><img src="./constantin_admin/upload/<?php echo $info->logo; ?>"/></li>
                                    <?php
                                endforeach;
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="section section-presentation">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 hidden-xs">
                            <div class="description">
                                <h4 class="header-text"><?php echo $ft[2]->title; ?></h4>
                                <p><?php echo $ft[2]->content_short; ?></p>
                                <p></p>
                                <p></p>
                                <ol>

                                </ol>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <img src="assets/img/showcases/showcase-2/mac2.png" style="margin-top:-50px"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-demo">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="demo-image">
                                <img src="assets/img/showcases/showcase-2/examples/home_4.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            <h4 class="header-text"><?php echo $ft[3]->title; ?></h4>
                            <p><?php echo $ft[3]->content_short; ?></p>
                            <p>

                            </p>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-5">
                            <h4 class="header-text"><?php echo $ft[4]->title; ?></h4>
                            <p><?php echo $ft[4]->content_short; ?></p>
                            <p>

                            </p>
                        </div>
                        <div class="col-md-6 col-md-offset-1">
                            <div class="demo-image">
                                <img src="assets/img/showcases/showcase-2/examples/home_6.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-features">
                <div class="container">
                    <h4 class="header-text text-center">Features</h4>
                    <div class="row">
                        <?php
                        if (count($fc) > 0) {
                            $fc_break=1;
                            foreach ($fc as $content):
                                ?>
                                <div class="col-md-4">
                                    <a href="">
                                        <div class="card card-blue">
                                            <div class="icon">
                                                <i class="<?php echo $content->icon_class; ?>"></i>
                                            </div>
                                            <div class="text">
                                                <h4 style="padding-bottom: 10px;"><strong><?php echo $content->name; ?></strong></h4>
                                                <p><?php echo $content->short_content; ?></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php
                                if ($fc_break == 3) {
                                    ?>
                                    <div class="clearfix"></div>
                                    <?php
                                }
                                $fc_break++;

                            endforeach;
                        }
                        ?>

                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php include('./include/fotter.php'); ?>
        </div>
    </body>
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.js" type="text/javascript"></script>
    <script src="assets/js/awesome-landing-page.js" type="text/javascript"></script>
    <script src="assets/js/login-register.js" type="text/javascript"></script>
    <script src="assets/js/ct-navbar.js"></script>
</html>