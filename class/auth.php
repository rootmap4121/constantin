<?php

if (empty(session_id())) {
    session_start();
}
$fullname='';
$input_by='';
include('db_Class.php');
$obj=new db_class();

//echo $_SESSION['SESS_CONST_USER_ID'];

if (!isset($_SESSION['SESS_CONST_USER_ID']) || trim($_SESSION['SESS_CONST_USER_ID']) == "") {
    echo $obj->Error("Login Session Expires, Please login again.", "index.php");
    exit();
}else {
    $input_by=$_SESSION['SESS_CONST_USER_ID'];
    $fullname=$_SESSION['SESS_CONST_FULL_NAME'];
}